#include <fstream>
#include <iostream>
#include <algorithm> 
#include <math.h> 


const int DATA_OFFSET_INFO = 0x000A; const int DATA_OFFSET_INFO_SIZE = 0x04;
const int WIDTH_INFO = 0x0012; const int WIDTH_INFO_SIZE = 0x04; 
const int HEIGHT_INFO = 0x0016; const int HEIGHT_INFO_SIZE = 0X04; 

char *IMG_FILENAME;
char *OUT_IMG_FILENAME; 
char *FILTERED_OUT_IMG_FILENAME; 
int MASK_CHOICE; 
int THRESHOLD = 0; 

const int WINDOW_SIZE = 3;
int LAPLACIAN_MASK[][3] = {{-1, -1, -1}, {-1, 8, -1}, {-1, -1, -1}};
int PREWITT_VERTICAL_MASK[][3] = {{-1, 0, 1}, {-1, 0, 1}, {-1, 0, 1}}; 
int PREWITT_HORIZONTAL_MASK[][3] = {{-1, -1, -1}, {0, 0, 0}, {1, 1, 1}}; 
int SOBEL_VERTICAL_MASK[][3] = {{-1, 0, 1}, {-2, 0, 2}, {-1, 0, 1}}; 
int SOBEL_HORIZONTAL_MASK[][3] = {{-1, -2, -1}, {0, 0, 0}, {1, 2, 1}}; 

int BITMAP_OFFSET=0; 
int IMAGE_DIMENSION_HEIGHT=0; 
int IMAGE_DIMENSION_WIDTH=0; 

using namespace std;

int main (int argc, char** argv) {

//  Obtain the command line arguments and process them accordingly 
//  The first argument must be the input filename 
//  The second argument must be the binary output filename 
//  The third argument must be the filter output filename 
//  The fourth argument must be the mask choice: 
//         0 for LAPLACIAN; 
//         1 for PREWITT_VERTICAL;
//         2 for PREWITT_HORIZONTAL; 
//         3 for SOBEL_VERTICAL; 
//         4 for SOBEL_HORIZONTAL; 
//         default is LAPLACIAN 
//  The fifth argument must be the THRESHOLD value (default is 200) 


    if (argc>=2) {
        IMG_FILENAME=argv[1];
        cout << "You have passed IMG_FILENAME: " << IMG_FILENAME << endl; 
    }   else {
        IMG_FILENAME="lena/lena512.bmp"; 
        cout << "Using default IMG_FILENAME: " << IMG_FILENAME << endl;  
    }

    if (argc>=3) {
        OUT_IMG_FILENAME=argv[2]; 
    }   else {
        OUT_IMG_FILENAME="edge_detection.bin.out.bmp";
        cout << "Using default value for OUT_IMG_FILENAME: " << OUT_IMG_FILENAME << endl; 
    }

    if (argc>=4) {
        FILTERED_OUT_IMG_FILENAME=argv[3];             
    }   else {
        FILTERED_OUT_IMG_FILENAME="edge_detection.filter.out.bmp";
        cout << "Using default value for FILTERED_OUT_IMG_FILENAME: " << FILTERED_OUT_IMG_FILENAME << endl; 
    } 

    if (argc>=5) {
        MASK_CHOICE=(int)(argv[4][0]-'0');
    }   else {
        MASK_CHOICE = 0; 
        cout << "Using default value for MASK_CHOICE: " << MASK_CHOICE << endl; 
    } 

    if (argc==6) {
        int i=0;
        while (argv[5][i]!='\0') {
            THRESHOLD=THRESHOLD*10 + (int)(argv[5][i++]-'0');
        } 
        
        cout << "You have passed THRESHOLD: " << THRESHOLD << endl; 
    }   else {
        THRESHOLD=128; 
        cout << "Using default value for THRESHOLD: " << THRESHOLD << endl; 
    } 
    

    ifstream infile;
    infile.open(IMG_FILENAME, ios::in);

    if (!infile.is_open()) {
        cout << "Error trying to open the file: " << IMG_FILENAME << "!\nExiting now!\n"; 
        return 0;
    }

    infile.seekg(0); 
    infile.seekg(0, infile.end);
    int length=infile.tellg();
    cout << "File Length: " << length << " bytes\n"; 

    int buffer; 
    infile.seekg(DATA_OFFSET_INFO, infile.beg); unsigned char offsetBuffer[DATA_OFFSET_INFO_SIZE];
    infile.read((char *) offsetBuffer, DATA_OFFSET_INFO_SIZE);
    for (int i=0; i<4; ++i) {
        buffer = (int) offsetBuffer[i]; 
        // cout << buffer << endl; 
        BITMAP_OFFSET += buffer*pow(256, i); 
    }   
    cout << "BITMAP_OFFSET: " << BITMAP_OFFSET << endl; 
    
    infile.seekg(WIDTH_INFO, infile.beg); unsigned char widthInfoBuffer[WIDTH_INFO_SIZE];
    infile.read((char *) widthInfoBuffer, WIDTH_INFO_SIZE); 
    for (int i=0; i<4; ++i) {
        buffer = (int) widthInfoBuffer[i]; 
        // cout << buffer << endl; 
        IMAGE_DIMENSION_WIDTH += buffer*pow(256, i); 
    }
    cout << "IMAGE_DIMENSION_WIDTH: " << IMAGE_DIMENSION_WIDTH << endl; 

    infile.seekg(HEIGHT_INFO, infile.beg); unsigned char heightInfoBuffer[HEIGHT_INFO_SIZE];
    infile.read((char *) heightInfoBuffer, HEIGHT_INFO_SIZE);   
    for (int i=0; i<4; ++i) {
        buffer = (int) heightInfoBuffer[i];
        // cout << buffer << endl; 
        IMAGE_DIMENSION_HEIGHT += buffer*pow(256, i); 
    } 
    cout << "IMAGE_DIMENSION_HEIGHT: " << IMAGE_DIMENSION_HEIGHT << endl; 


    infile.seekg(0, infile.beg);
    char bitmapOffsetData[BITMAP_OFFSET];
    infile.read(bitmapOffsetData, BITMAP_OFFSET);

    unsigned char bitmapData[IMAGE_DIMENSION_HEIGHT][IMAGE_DIMENSION_WIDTH];
    infile.seekg(BITMAP_OFFSET); 
    for (int i=0; i<IMAGE_DIMENSION_HEIGHT; ++i) {
            infile.read((char *) bitmapData[i], IMAGE_DIMENSION_WIDTH);
    }

    int paddedBitmapPicture[IMAGE_DIMENSION_HEIGHT+2][IMAGE_DIMENSION_WIDTH+2]; int maxValue=-256; int minValue=256;
    for (int i=0; i<IMAGE_DIMENSION_HEIGHT; ++i) {
        for (int j=0; j<IMAGE_DIMENSION_WIDTH; ++j) {
            if (i==0 || j==0) {
                paddedBitmapPicture[i][j]=0;
            }   else if (i==IMAGE_DIMENSION_HEIGHT-1 || j==IMAGE_DIMENSION_WIDTH-1) {
                paddedBitmapPicture[i+2][j+2]=0;
            }
            paddedBitmapPicture[i+1][j+1] = bitmapData[i][j];
            // cout << i << ", " << j << ", " << paddedBitmapPicture[i+1][j+1] << "\t";
            if (paddedBitmapPicture[i+1][j+1]>maxValue) {
                maxValue = paddedBitmapPicture[i+1][j+1];
            }
            if (paddedBitmapPicture[i+1][j+1]<minValue) {
                minValue = paddedBitmapPicture[i+1][j+1];
            }
        }
        // cout << endl;
    }

    cout << "Max Value: " << maxValue << ", Min Value: " << minValue << endl;
    infile.close();

    int mask[3][3]; 
    switch (MASK_CHOICE) {
        case 0:  copy(&LAPLACIAN_MASK[0][0], &LAPLACIAN_MASK[0][0]+WINDOW_SIZE*WINDOW_SIZE, &mask[0][0]); break; 
        case 1:  copy(&PREWITT_VERTICAL_MASK[0][0], &PREWITT_VERTICAL_MASK[0][0]+WINDOW_SIZE*WINDOW_SIZE, &mask[0][0]); break; 
        case 2:  copy(&PREWITT_HORIZONTAL_MASK[0][0], &PREWITT_HORIZONTAL_MASK[0][0]+WINDOW_SIZE*WINDOW_SIZE, &mask[0][0]); break; 
        case 3:  copy(&SOBEL_VERTICAL_MASK[0][0], &SOBEL_VERTICAL_MASK[0][0]+WINDOW_SIZE*WINDOW_SIZE, &mask[0][0]); break; 
        case 4:  copy(&SOBEL_HORIZONTAL_MASK[0][0], &SOBEL_HORIZONTAL_MASK[0][0]+WINDOW_SIZE*WINDOW_SIZE, &mask[0][0]); break; 
        default: copy(&LAPLACIAN_MASK[0][0], &LAPLACIAN_MASK[0][0]+WINDOW_SIZE*WINDOW_SIZE, &mask[0][0]); break; 
    }

    // Check for mask 
    cout <<"APPLYING THE FOLLOWING MASK:\n";
    for (int i=0; i<WINDOW_SIZE; ++i) {
        for (int j=0; j<WINDOW_SIZE; ++j) 
            cout << mask[i][j];
        cout << endl;
    }

    // Checks for paddedBitmapPicture 

    // int sum1=0, sum2=0, sum3=0, sum4=0; 
    // for (int i=0; i<IMAGE_DIMENSION_HEIGHT+2; ++i) {
    //     sum2+=paddedBitmapPicture[i][0];
    //     sum4+=paddedBitmapPicture[i][IMAGE_DIMENSION_WIDTH+1];
    // }

    // for (int j=0; j<IMAGE_DIMENSION_WIDTH+2; ++j) {
    //     sum1+=paddedBitmapPicture[0][j];
    //     sum3+=paddedBitmapPicture[IMAGE_DIMENSION_HEIGHT+1][j];
    // }

    // cout << sum1 << sum2 << sum3 << sum4 << endl;
    
    int filteredImage[IMAGE_DIMENSION_HEIGHT][IMAGE_DIMENSION_WIDTH]; 
    int imageMaskBuf[WINDOW_SIZE][WINDOW_SIZE];
    
    int midWindowSize;
    if (WINDOW_SIZE%2) {
        midWindowSize=(WINDOW_SIZE-1)/2;
    }   else {
        midWindowSize=(WINDOW_SIZE)/2;
    }

    for (int i=1; i<IMAGE_DIMENSION_HEIGHT+1; ++i) {
        for (int j=1; j<IMAGE_DIMENSION_WIDTH+1; ++j) {
            filteredImage[i-1][j-1]=0;
            for (int k=0; k<WINDOW_SIZE; ++k) {
                for (int l=0; l<WINDOW_SIZE; ++l) {
                    imageMaskBuf[k][l]=mask[k][l]*paddedBitmapPicture[i-(midWindowSize-k)][j-(midWindowSize-l)];
                    filteredImage[i-1][j-1]+=imageMaskBuf[k][l];
                }
            }

            // Clipping 
            filteredImage[i-1][j-1]=min(filteredImage[i-1][j-1], 255);
            filteredImage[i-1][j-1]=max(filteredImage[i-1][j-1], 0);

            // cout << i-1 << ", " << j-1 << ", " << filteredImage[i-1][j-1] << "\t";
        }
    }

    ofstream outfile;
    outfile.open(FILTERED_OUT_IMG_FILENAME, ios::out);

    // Write the same header information 
    outfile.write(bitmapOffsetData, BITMAP_OFFSET);

    for (int i=0; i<IMAGE_DIMENSION_HEIGHT; ++i) {
        for (int j=0; j<IMAGE_DIMENSION_WIDTH; ++j) {
            outfile.put((char)filteredImage[i][j]);
        }
    }

    outfile.close();

    // Thresholding 
    // cout << "Filtered Image: " << endl; 

    for (int i=0; i<IMAGE_DIMENSION_HEIGHT; ++i) {
        for (int j=0; j<IMAGE_DIMENSION_WIDTH; ++j) {
            // cout << filteredImage[i][j] << endl;
            if (filteredImage[i][j]>=THRESHOLD) {
                filteredImage[i][j]=255;
            }   else {
                filteredImage[i][j]=0;
            }
        }
    }
    
    outfile.open(OUT_IMG_FILENAME, ios::out);

    // Write the same header information 
    outfile.write(bitmapOffsetData, BITMAP_OFFSET);

    for (int i=0; i<IMAGE_DIMENSION_HEIGHT; ++i) {
        for (int j=0; j<IMAGE_DIMENSION_WIDTH; ++j) {
            outfile.put((char)filteredImage[i][j]);
        }
    }

    outfile.close();

    return 0;
}
