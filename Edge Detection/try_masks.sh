#!/bin/bash 
map=("Laplacian" "Prewitt_V" "Prewitt_H" "Sobel_V" "Sobel_H")
for ii in {0..4}; do
	./main.cpp.o Vishnu/Vishnu.bmp Vishnu/Vishnu_${map[$ii]}.bin.out.bmp Vishnu/Vishnu_${map[$ii]}.filt.out.bmp ${ii}
	./main.cpp.o CSS/CSS2.bmp CSS/CSS2_${map[$ii]}.bin.out.bmp CSS/CSS2_${map[$ii]}.filt.out.bmp ${ii}
	./main.cpp.o lena/lena512.bmp lena/lena_${map[$ii]}.bin.out.bmp lena/lena_${map[$ii]}.filt.out.bmp ${ii}
done

