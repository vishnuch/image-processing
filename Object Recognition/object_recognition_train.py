import tensorflow as tf 
from tensorflow import keras 
import time 
import numpy as np 
import matplotlib.pyplot as plt 

MODEL_FILENAME = 'NN_model.h5' 
NUM_EPOCHS = 500 

print("Training for "+str(NUM_EPOCHS)+" epochs")
print("Output model filename: " + MODEL_FILENAME) 

print("Loading data")
fashion_mnist = keras.datasets.fashion_mnist 
(train_images, train_labels), (test_images, test_labels) = fashion_mnist.load_data() 

class_names = ['T-shirt/top', 'Trouser', 'Pullover', 'Dress', 'Coat', 
               'Sandal', 'Shirt', 'Sneaker', 'Bag', 'Ankle boot'] 

print("Loaded Data")

fig=plt.figure(figsize=(10,10))
for i in range(10):
    plt.subplot(5,2,i+1)
    plt.xticks([])
    plt.yticks([])
    plt.imshow(train_images[i], cmap=plt.cm.binary)
    plt.xlabel(class_names[train_labels[i]])
    plt.grid(False)
plt.show() 
time.sleep(5)
plt.close(fig)

print("Creating model")
model = keras.Sequential([keras.layers.Flatten(input_shape=(28, 28)), keras.layers.Dense(128, activation=tf.nn.relu), keras.layers.Dense(32, activation=tf.nn.sigmoid), keras.layers.Dense(16, activation=tf.nn.sigmoid), keras.layers.Dense(10, activation=tf.nn.softmax)]) 

print("Compiling model")
model.compile(optimizer="adam", loss="sparse_categorical_crossentropy", metrics=['accuracy']) 
print ("Compiled model") 

print("Running "+str(NUM_EPOCHS)+" epochs of training now!")
model.fit(train_images, train_labels, epochs=NUM_EPOCHS) 

print("Saving Model")
model.save(MODEL_FILENAME)
print('Model has been saved to file: ', MODEL_FILENAME)
 

